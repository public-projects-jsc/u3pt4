package dam.androidjaviersolercanto.u3t4event;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST = 0;

    private EditText etEventName;
    private TextView tvCurrentData;

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Si el bundle no está intanciado lo instanciamos
         */

        if (bundle == null) bundle = new Bundle();

        setUI();
    }

    private void setUI() {
        etEventName = findViewById(R.id.etEventName);
        tvCurrentData = findViewById(R.id.tvCurrentData);
    }

    public void editEventData(View view) {
        Intent intent = new Intent(this, EventDataActivity.class);

        bundle.putString("EventName", etEventName.getText().toString());

        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /**
         * Si la salida de la anterior activity ha sido correcta, guardamos los datos
         */

        if (requestCode == REQUEST && resultCode == RESULT_OK) {

            bundle = data.getExtras();

            tvCurrentData.setText(
                    getString(R.string.place, data.getStringExtra("place")) + "\n" +
                    getString(R.string.priority, data.getStringExtra("priority")) + "\n" +
                    getString(R.string.complete_date, data.getStringExtra("date")) + "\n" +
                    getString(R.string.hour, data.getStringExtra("hour"))
            );
        }
    }

    /**
     * En caso de cambio de configuración recuperamos los datos
     * @param outState
     */

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        /**
         * Guardamos el bundle entero para recuperarlo luego
         */

        outState.putBundle("bundle",bundle);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        /**
         * Recuperamos el bundle y sus valores
         */

        bundle = savedInstanceState.getBundle("bundle");
        tvCurrentData.setText(
                        getString(R.string.place, bundle.getString("place")) + "\n" +
                        getString(R.string.priority, bundle.getString("priority")) + "\n" +
                        getString(R.string.complete_date, bundle.getString("date")) + "\n" +
                        getString(R.string.hour, bundle.getString("hour"))
        );
    }
}
