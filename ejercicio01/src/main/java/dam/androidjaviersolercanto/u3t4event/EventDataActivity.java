package dam.androidjaviersolercanto.u3t4event;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String selectedPriority = "Normal";

    private TextView tvEventName;
    private EditText etPlace;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        // TODO Ex1: Recuperamos los datos de la activity anterior para inicializar los campos

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));
        etPlace.setText(inputData.getString("place"));
        ((RadioButton)rgPriority.getChildAt(whichSelected(inputData.getString("priority")))).setChecked(true);
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        etPlace = findViewById(R.id.etPlace);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        // TODO Ex1: Recuperamos los datos anteriores en caso de que se pulse CANCEL

        /**
         * Creamos el bundle para recuperar los datos de la anterior Activity
         */

        Bundle inputData = getIntent().getExtras();

        switch (view.getId()) {
            case R.id.btAccept:

                // TODO Ex1: Creamos el array desde los recursos y lo cargamos en el OnCreate

                /**
                 * Cargamos el array desde los recursos
                 */

                String[] months = getResources().getStringArray(R.array.months);

                /**
                 * Añadimos el place y la hora al texto
                 */

                eventData.putString("place", etPlace.getText().toString());
                eventData.putString("priority", selectedPriority);
                eventData.putString("date", dpDate.getDayOfMonth() + " " + months[dpDate.getMonth()]
                        + " " + dpDate.getYear());
                eventData.putString("hour", tpTime.getHour() + ":" + tpTime.getMinute());

                activityResult.putExtras(eventData);
                setResult(RESULT_OK, activityResult);

            case R.id.btCancel:

                /**
                 * Si el botón pulsado ha sido el de cancelar recogemos el currentData
                 * para que sea el mismo que el de antes de acceder a la Activity
                 */

                eventData.putString("place",inputData.getString("place"));
                eventData.putString("priority",inputData.getString("priority"));
                eventData.putString("date",inputData.getString("date"));
                eventData.putString("hour",inputData.getString("hour"));
                break;
        }

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rbLow:
                selectedPriority = getResources().getString(R.string.low);
                break;
            case R.id.rbNormal:
                selectedPriority = getResources().getString(R.string.normal);
                break;
            case R.id.rbHigh:
                selectedPriority = getResources().getString(R.string.high);
                break;
        }
    }

    /**
     * Éste método permite saber que opción estaba seleccionada en el evento anterior
     * @param selected
     * @return
     */

    public int whichSelected(String selected) {
        if (selected != null) {
            if (selected.equals(getString(R.string.low))) {
                return 0;
            } else if (selected.equals(getString(R.string.high))) {
                return 2;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }
}
