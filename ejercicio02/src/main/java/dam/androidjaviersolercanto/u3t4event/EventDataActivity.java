package dam.androidjaviersolercanto.u3t4event;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String selectedPriority = "Normal";

    private TextView tvEventName;
    private EditText etPlace;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        etPlace = findViewById(R.id.etPlace);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate = findViewById(R.id.dpDate);
        tpTime = findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

        // TODO Ex2: Mostrar o no dependiendo de la rotación el calendario

        if (getRotation(this).equals("vertical")) {
            dpDate.setCalendarViewShown(false);
        } else {
            dpDate.setCalendarViewShown(true);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        Bundle inputData = getIntent().getExtras();

        switch (view.getId()) {
            case R.id.btAccept:
                String[] months = getResources().getStringArray(R.array.months);
                eventData.putString("place", etPlace.getText().toString());
                eventData.putString("priority", selectedPriority);
                eventData.putString("date", dpDate.getDayOfMonth() + " " + months[dpDate.getMonth()]
                        + " " + dpDate.getYear());
                eventData.putString("hour", tpTime.getHour() + ":" + tpTime.getMinute());

                activityResult.putExtras(eventData);
                setResult(RESULT_OK, activityResult);
                break;
            case R.id.btCancel:
                eventData.putString("EventData", inputData.getString("CurrentData"));
                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rbLow:
                selectedPriority = getResources().getString(R.string.low);
                break;
            case R.id.rbNormal:
                selectedPriority = getResources().getString(R.string.normal);
                break;
            case R.id.rbHigh:
                selectedPriority = getResources().getString(R.string.high);
                break;
        }
    }

    public String getRotation(Context context){

        /**
         * Recogemos el valor que nos muestra la orientación de la pantalla. Nos devolverá el numero de grados que dispone la pantalla
         */

        int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();

        /**
         * Si está en 0º o 180º querra decir que está en vertical si es 90 u otro querrá decir que está apaisado
         */

        if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) {
            return "vertical";
        }

        return "horizontal";
    }
}
