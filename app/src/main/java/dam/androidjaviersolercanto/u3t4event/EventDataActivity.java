package dam.androidjaviersolercanto.u3t4event;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private String selectedPriority = "Normal";

    private TextView tvEventName;
    private TextView tvDatePicker;
    private TextView tvTimePicker;
    private EditText etPlace;
    private RadioGroup rgPriority;
    private Button btAccept;
    private Button btCancel;

    private int monthPicked;
    private int dayPicked;
    private int yearPicked;
    private String hourPicked;
    private String minutePicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);

        setUI();

        Bundle inputData = getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));
        tvTimePicker.setText(inputData.getString("hour"));
        tvDatePicker.setText(inputData.getString("date"));
        etPlace.setText(inputData.getString("place"));
    }

    private void setUI() {

        tvEventName = findViewById(R.id.tvEventName);
        tvDatePicker = findViewById(R.id.tvDatePicker);
        tvTimePicker = findViewById(R.id.tvTimePicker);
        etPlace = findViewById(R.id.etPlace);
        rgPriority = findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        btAccept = findViewById(R.id.btAccept);
        btCancel = findViewById(R.id.btCancel);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);

        getToday();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        Bundle inputData = getIntent().getExtras();

        switch (view.getId()) {
            case R.id.btAccept:
                String[] months = getResources().getStringArray(R.array.months);

                eventData.putString("place", etPlace.getText().toString());
                eventData.putString("priority", selectedPriority);
                eventData.putString("date", dayPicked + " " + months[monthPicked] + " " + yearPicked);
                eventData.putString("hour", hourPicked + ":" + minutePicked);
                break;
            case R.id.btCancel:
                eventData.putString("place",inputData.getString("place"));
                eventData.putString("priority",inputData.getString("priority"));
                eventData.putString("date",inputData.getString("date"));
                eventData.putString("hour",inputData.getString("hour"));
                break;
        }

        activityResult.putExtras(eventData);
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rbLow:
                selectedPriority = getResources().getString(R.string.low);
                break;
            case R.id.rbNormal:
                selectedPriority = getResources().getString(R.string.normal);
                break;
            case R.id.rbHigh:
                selectedPriority = getResources().getString(R.string.high);
                break;
        }
    }

    // TODO Ex4: Realizamos la recogida de fecha y hora mediante Pickers para ahorrar espacio

    public void showCalendar(View view){

        /**
         * Creamos la variable cal que nos permite acceder a la fecha actual
         */

        Calendar cal = Calendar.getInstance();

        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int year = cal.get(Calendar.YEAR);

        /**
         * Creamos el Picker correspondiente a la fecha
         */

        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            /**
             * Éste método nos permite saber que dia, mes y año han sido seleccionados
             * @param view
             * @param year
             * @param month
             * @param dayOfMonth
             */

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                monthPicked = month++;      // Como el mes empieza por 0 (0-11) sumaremos 1 siempre
                dayPicked = dayOfMonth;
                yearPicked = year;

                /**
                 * Guardamos en el tv correspondiente los datos seleccionados
                 */

                tvDatePicker.setText(dayPicked + " " + getResources().getStringArray(R.array.months)[monthPicked] + " " + yearPicked);
            }
        },year, month, day);

        datePicker.show();
    }

    public void showClock(View view){

        Calendar cal = Calendar.getInstance();

        /**
         * Ahora solo recogemos la hora y el minuto
         */

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);

        TimePickerDialog timePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                hourPicked = String.valueOf(hourOfDay);
                minutePicked = String.valueOf(minute);

                /**
                 * Si el número es menor a 10 le pondremos un 0 delante por estética de visualización
                 */

                if (hourOfDay < 10) {
                    hourPicked = "0" + hourPicked;
                }

                if (minute < 10) {
                    minutePicked = "0" + minutePicked;
                }

                tvTimePicker.setText(hourPicked + ":" + minutePicked);
            }
        }, hour, minute, true);

        timePicker.show();
    }

    public void getToday() {

        Calendar cal = Calendar.getInstance();

        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int year = cal.get(Calendar.YEAR);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);

        dayPicked = day;
        monthPicked = month++;
        yearPicked = year;
        hourPicked = String.valueOf(hour);
        minutePicked = String.valueOf(minute);

        if (hour < 10) {
            hourPicked = "0" + hourPicked;
        }

        if (minute < 10) {
            minutePicked = "0" + minutePicked;
        }
    }
}
